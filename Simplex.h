#ifndef SIMPLEX_H
#define SIMPLEX_H

#include "Tableau.h"

enum METHOD{TWO_PHASE, BIG_M, ONE_ARTIFICIAL, REVISED, DUAL};
enum CONSTRAINT{GREATER, LESS, EQUAL};

class Simplex
{

private:
	Tableau * tableau;
	double * objective;
	double * value;
	int * constraint;
	int method;
	int v1;
	int v2;

	void transform();
	void removeColumn(int column, double ** mat);

public:
	Simplex(int v1, int v2);
	void setValues(double * objective, double * value, int * constraint, int method);
	double getSolution();
	void solve();

	virtual ~Simplex();

};

#endif