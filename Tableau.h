#ifndef TABLEAU_H
#define TABLEAU_H

#include <limits>
#include <algorithm>

#define MAX_ITERATIONS 20

class Tableau
{
public:
	void solveTableau();
	double ** getTableau(int iteration);
	void fillTableau(double * data);
	int getIterations();

	Tableau(int v1, int v2);
	virtual ~Tableau();

private:
	double ** tableau;
	double *** log;
	int v1, v2, final_i;

	bool test();
	double ** getTableau();
	void copyTableau(double ** o, double *** d);
	void update(int row, int column);
	int getPivotRow(int pivot_column);
	int getPivotColumn();
	void allocateTableauSimplex();
	void allocateLog();
	void deleteTableau();
	void deleteLog();

};

#endif