#include "Tableau.h"

Tableau::Tableau(int v1, int v2)
{
	this->v1 = v1;
	this->v2 = v2;

	allocateTableauSimplex();
	allocateLog();
}

Tableau::~Tableau()
{
	deleteTableau();
    deleteLog();
}

void Tableau::copyTableau(double ** o, double *** d)
{
    std::copy(&(o[0][0]), &(o[v2][v1 + v2 + 3]), &((*d)[0][0]));
}

void Tableau::solveTableau()
{
    for(final_i = 0; !test() && final_i < MAX_ITERATIONS; final_i++)
    {
        copyTableau(getTableau(), &(log[final_i]));

        int in_v = getPivotColumn();
        int out_v = getPivotRow(in_v);

        update(out_v, in_v);
    };

    copyTableau(getTableau(), &(log[final_i++]));
}

int Tableau::getIterations()
{
    return final_i;
}

double ** Tableau::getTableau()
{
    return tableau;
}

double ** Tableau::getTableau(int iteration)
{
	return log[iteration];
}

bool Tableau::test()
{
    if(final_i == 0) return false;
	for(int i = 2; i < v1 + v2 + 2; i++)
        if(tableau[0][i] < 0)
            return false;
    return true;
}

void Tableau::update(int row, int column)
{
    tableau[row][0] = column - 1;

    double pivot = tableau[row][column];

    for(int i = 2; i < v1 + v2 + 3; i++)
        tableau[row][i] /= pivot;

    for(int i = 0; i < v2 + 1; i++)
    {
        pivot = tableau[i][column];
        if(i != row)
            for(int j = 2; j < v1 + v2 + 3; j++)
                tableau[i][j] -= tableau[row][j] * pivot;
    }
}

int Tableau::getPivotRow(int pivot_column)
{
	double min = std::numeric_limits<double>::infinity();
    int index;

    for(int i = 1; i < v2 + 1; i++)
        if(tableau[i][pivot_column] > 0 && tableau[i][v1 + v2 + 2] / tableau[i][pivot_column] < min)
        {
            min = tableau[i][v1 + v2 + 2] / tableau[i][pivot_column];
            index = i;
        }

    return index;
}

int Tableau::getPivotColumn()
{
	double min = std::numeric_limits<double>::infinity();
    int index;

    for(int i = 2; i < v1 + v2 + 2; i++)
        if(tableau[0][i] < min)
        {
            min = tableau[0][i];
            index = i;
        }

    return index;
}

void Tableau::allocateTableauSimplex()
{
	tableau = new double*[v2 + 1];
    for(int i = 0; i < v2 + 1; i++)
        tableau[i] = new double[v1 + v2 + 3];
}

void Tableau::allocateLog()
{
	log = new double**[MAX_ITERATIONS];
	for(int i = 0; i < MAX_ITERATIONS; i++)
	{
		log[i] = new double*[v2 + 1];
    	for(int j = 0; j < v2 + 1; j++)
        	log[i][j] = new double[v1 + v2 + 3];
	}
}

void Tableau::fillTableau(double * data)
{
	for(int i = 1; i < v2 + 1; i++)
        tableau[i][0] = v1 + i;

    int counter = 0;
    for(int i = 0; i < v2 + 1; i++)
        for(int j = 2; j < v1 + v2 + 3; j++)
            tableau[i][j] = data[counter++];
}

void Tableau::deleteTableau()
{
	for(int i = 0; i < v2 + 1; i++)
        delete tableau[i];
    delete tableau;
}

void Tableau::deleteLog()
{
	for(int i = 0; i < MAX_ITERATIONS; i++)
    	for(int j = 0; j < v2 + 1; j++)
        	delete log[i][j];

	for(int i = 0; i < MAX_ITERATIONS; i++)
        delete log[i];
    
	delete log;
}