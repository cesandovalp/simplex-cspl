#include "Simplex.h"
#include <iostream>
#include <stdlib.h>

/*
 * Z = 4x1 + x2 + 6x3
 * -2x1 - x2 + 2x3 >= 1
 * x1 + x2 + x3 >= 6
 *
 * x1, x2, x3 >= 0
 */

void test1()
{
    int variables = 3;
    int restricciones = 2;

    Simplex simplex(variables, restricciones);

    double * objectivo = new double[variables]{4, 1, 6};
    int * simbolos = new int[restricciones]{GREATER, GREATER};
    double * valores = new double[(variables + 1) * restricciones]{-2, -1, 2, 1,
                                                                    1,  1, 1, 6};
    simplex.setValues(objectivo, valores, simbolos, TWO_PHASE);
    simplex.solve();
}

void test2()
{
//    int variables = 2;
//    int restricciones = 3;
    int variables = 4;
    int restricciones = 2;

    Tableau test(variables, restricciones);

//    double * valores = new double[4*6]{0, -3, -5, 0, 0,  0,
//                                       0,  1,  0, 1, 0,  4,
//                                       1,  0,  2, 0, 0, 12,
//                                       0,  3,  2, 0, 1, 18};
    double * valores = new double[3*7]{ -5, -9,  1,  1,  0,  0, -96,
                                         2,  3, -1,  0,  1,  0,  36,
                                         3,  6,  0, -1,  0,  1,  60};
    test.fillTableau(valores);

    test.solveTableau();

    std::cout.precision(2);

    for(int iteracion = 0; iteracion < test.getIterations(); iteracion++)
    {
        double ** r = test.getTableau(iteracion);

        std::cout << std::endl << std::endl;

        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 9; j++)
                std::cout << r[i][j] << "\t";
            std::cout << std::endl;
        }
    }

    delete valores;
}

int main(int argc, char ** argv)
{
    if(atoi(argv[1]) == 1)test1();
    if(atoi(argv[1]) == 2)test2();

    return 0;
}
