#include "Simplex.h"
#include <iostream>

Simplex::Simplex(int v1, int v2)
{
	this->v1 = v1;
	this->v2 = v2;

	objective = new double[v1];
	value = new double[(v1 + 1) * v2];
	constraint = new int[v2];
}

Simplex::~Simplex()
{
	//delete tableau;
}

void Simplex::setValues(double * objective, double * value, int * constraint, int method)
{
	this->method = method;

	std::copy(&(objective[0]), &(objective[v1]), &(this->objective[0]));
	std::copy(&(value[0]), &(value[((v1 + 1) * v2)]), &(this->value[0]));
	std::copy(&(constraint[0]), &(constraint[v2]), &(this->constraint[0]));
}

double Simplex::getSolution()
{
	;
}

void Simplex::solve()
{
	transform();
}

void Simplex::removeColumn(int column, double ** mat)
{
	;
}

void Simplex::transform()
{
	if(method == TWO_PHASE)
	{
		int artificial1 = 0, artificial2 = 0, slack = 0;

		for(int i = 0; i < v2; i++)
			if(constraint[i] == GREATER)
				artificial1++;
			else if(constraint[i] == EQUAL)
				artificial2++;
			else
				slack++;

		tableau = new Tableau(v1 + artificial1 + slack, v2);

		int width = (v1 + (artificial1*2) + artificial2 + slack + 1);
		int size = (v2 + 1) * width;
		double * data = new double[size];

		for(int i = width - 2; i >= width - artificial2 - artificial1 - 1; i--)
			data[i] = 1;

		//std::cout << std::endl;

		for(int i = 1, index = 0; i < v2 + 1; i++)
			for(int j = 0; j < width; j++)
				data[(i*width) + j] = j - v1 < 0 || j - width == -1 ? value[index++] : 
				       i - 1 == j - v1 ? -1 :
				       constraint[i - 1] == GREATER && i - 1 + v2 == j - v1 ? 1 : 0;

		//for(int i = 0; i < v2 + 1; i++, std::cout << std::endl){std::cout << "\t\t";
		//	for(int j = 0; j < width; j++)
		//		std::cout << data[(i * width) + j] << "\t";}

		for(int i = 1; i < v2 + 1; i++)
			for(int j = 0; j < width; j++)
				data[j] -= data[(i*width) + j];

		//for(int i = 0; i < v2 + 1; i++, std::cout << std::endl){std::cout << "\t\t";
		//	for(int j = 0; j < width; j++)
		//		std::cout << data[(i * width) + j] << "\t";}

		tableau->fillTableau(data);

		tableau->solveTableau();

		// for(int iteracion = tableau->getIterations()-1; iteracion < tableau->getIterations(); iteracion++)
	 //    {
	 //        double ** r = tableau->getTableau(iteracion);

	 //        std::cout << std::endl << std::endl;

	 //        for(int i = 0; i < 3; i++)
	 //        {
	 //            for(int j = 0; j < 10; j++)
	 //                std::cout << r[i][j] << "\t";
	 //            std::cout << std::endl;
	 //        }
	 //    }

		return;
	}

	if(method == BIG_M);
	{
		tableau = new Tableau(v1,v2);
		return;
	}

	if(method == ONE_ARTIFICIAL)
	{
		tableau = new Tableau(v1,v2);
		return;
	}

    if(method == REVISED)
    {
    	tableau = new Tableau(v1,v2);
    	return;
    }

    if(method == DUAL)
    {
    	tableau = new Tableau(v1,v2);
    	return;
    }
}